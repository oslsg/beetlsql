package org.beetl.sql.saga.common;

public interface SagaContextFactory {
	 SagaContext current();
}
